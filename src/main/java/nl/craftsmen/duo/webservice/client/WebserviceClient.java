package nl.craftsmen.duo.webservice.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import javax.net.ssl.SSLContext;

import lombok.extern.slf4j.Slf4j;
import nl.craftsmen.duo.webservice.data.Bedrijfsdocument;
import nl.craftsmen.duo.webservice.data.ConstantValues;
import nl.craftsmen.duo.webservice.data.WebserviceResponse;
import nl.craftsmen.duo.webservice.soaputils.AdressingVersionMustUnderstandDisabled;
import nl.craftsmen.duo.webservice.soaputils.ConnectionPoolConfiguration;
import nl.craftsmen.duo.webservice.soaputils.DuoXwsSecurityInterceptor;
import nl.craftsmen.duo.webservice.soaputils.EndPointInterceptor;
import nl.craftsmen.duo.webservice.soaputils.SoapConfiguration;
import nl.craftsmen.duo.webservice.soaputils.SoapXMLHelper;
import nl.craftsmen.duo.webservice.soaputils.SslConfiguration;
import nl.craftsmen.duo.webservice.soaputils.WebserviceMessageSender;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.NoopUserTokenHandler;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.addressing.client.ActionCallback;
import org.springframework.ws.soap.addressing.core.EndpointReference;
import org.springframework.ws.soap.security.xwss.XwsSecurityInterceptor;
import org.springframework.ws.transport.WebServiceMessageSender;

/**
 * Generic client for calling SSL-secured webservices using certificates.
 */
@Component
@Slf4j
public class WebserviceClient {
    public static final String HTTP_PROTOCOL = "http";
    public static final String HTTPS_PROTOCOL = "https";
    private static final String wsaPrefix = "http://www.w3.org/2005/08/addressing/anonymous?";

    public static final int CONNECT_TIMEOUT = 3000;
    public static final int READ_TIMEOUT = 14000;
    public static final int MAX_TOTAL_CONNECTIONS = 10;
    public static final int MAX_CONNECTIONS_PER_HOST = 10;


    private Jaxb2Marshaller marshaller;
    private Unmarshaller unmarshaller;
    private ConstantValues constantValues;
    private SslConfiguration sslConfiguration = new SslConfiguration();
    private XwsSecurityInterceptor xwsSecurityInterceptor;

    @Autowired
    public WebserviceClient(Jaxb2Marshaller marshaller, ConstantValues constantValues, DuoXwsSecurityInterceptor xwsSecurityInterceptor) {
        this.marshaller = marshaller;
        this.unmarshaller = marshaller;
        this.constantValues = constantValues;
        this.xwsSecurityInterceptor = xwsSecurityInterceptor;
    }

    public WebserviceResponse send(Object request, String fromURL) {
        String identificatie = ((Bedrijfsdocument) request).getIdentificatiecodeBedrijfsdocument();
        try {
            WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
            webServiceTemplate.setMarshaller(marshaller);
            webServiceTemplate.setMessageSender(createWebserviceMessageSender());
            webServiceTemplate.setInterceptors(addInterceptors());
            webServiceTemplate.setUnmarshaller(unmarshaller);
            webServiceTemplate.setDefaultUri(constantValues.getDestinationURL());

            ActionCallback actionCallback = createActionCallback(fromURL);
            Bedrijfsdocument response = callWebservice(webServiceTemplate, actionCallback, request);
            String soapRequestXml = SoapXMLHelper.getSoapRequestXml(identificatie);
            String soapResponseXml = SoapXMLHelper.getSoapResponseXml(identificatie);
            return new WebserviceResponse(response, soapRequestXml, soapResponseXml);
        } catch (Exception exception) {
            String soapRequestXml = SoapXMLHelper.getSoapRequestXml(identificatie);
            String soapResponseXml = SoapXMLHelper.getSoapResponseXml(identificatie);
            log.error("WebserviceClient call ended in error", exception);
            return new WebserviceResponse(new Bedrijfsdocument().withIdentificatiecodeBedrijfsdocument(identificatie), soapRequestXml, soapResponseXml);
        }
    }

    private WebServiceMessageSender createWebserviceMessageSender() throws Exception {
        Registry<ConnectionSocketFactory> registry = createRegistry();

        SoapConfiguration soapConfiguration = SoapConfiguration.builder()
                .sslConfiguration(sslConfiguration)
                .connectionPoolConfiguration(createConnectionPoolConfig())
                .destinationUrl(constantValues.getDestinationURL())
                .build();

        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .addInterceptorFirst(new HttpRequestInterceptor() {

                    /* The header already contained the Content-length parameter so when present it will be removed and later
                    * automatically added. If we omit this part the call will fail */
                    @Override
                    public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
                        if (httpRequest.containsHeader(HTTP.CONTENT_LEN)) {
                            httpRequest.removeHeaders(HTTP.CONTENT_LEN);
                        }
                    }
                })
                .setUserTokenHandler(NoopUserTokenHandler.INSTANCE)
                .setConnectionManager(createConnectionManager(registry, soapConfiguration))
                .setDefaultRequestConfig(RequestConfig.custom().setConnectTimeout(CONNECT_TIMEOUT).build())
                .build();
        return new WebserviceMessageSender(httpClient);

    }

    /**
     * Sets the timeout settings and the max number of connections for the webservice to be called
     * @return a connection pool configuration
     */
    private ConnectionPoolConfiguration createConnectionPoolConfig() {
        return ConnectionPoolConfiguration.builder()
                .connectTimeout(CONNECT_TIMEOUT)
                .readTimeout(READ_TIMEOUT)
                .maxTotalConnections(MAX_TOTAL_CONNECTIONS)
                .maxConnectionsPerHost(MAX_CONNECTIONS_PER_HOST)
                .build();
    }

    /**
     * Create a registry with the protocols allowed (http and https)
     * @return the registry with the allowed protocols
     * @throws Exception an exception when an error occurs
     */
    private Registry<ConnectionSocketFactory> createRegistry() throws Exception {
        SSLContext sslContext = createSSLContext();
        SSLConnectionSocketFactory https = new SSLConnectionSocketFactory(sslContext, null, sslConfiguration.getSupportedCiphers(), sslConfiguration.getHostnameVerifier());

        RegistryBuilder<ConnectionSocketFactory> builder = RegistryBuilder.create();
        return builder
                .register(HTTP_PROTOCOL, PlainConnectionSocketFactory.getSocketFactory())
                .register(HTTPS_PROTOCOL, https)
                .build();
    }

    private SSLContext createSSLContext() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        return SSLContexts.custom()
                .loadKeyMaterial(sslConfiguration.getKeystore(), sslConfiguration.getKeyPassword())
                .loadTrustMaterial(sslConfiguration.getTruststore(), null)
                .setSecureRandom(new SecureRandom())
                .useProtocol(sslConfiguration.getProtocol())
                .build();
    }

    /**
     * Creates 2 interceptors:
     *      1. Intercept the message and extract the soap/xml request and response
     *      2. Add a security interceptor for adding security elements to message and sign the message
     * @return a list of interceptors
     * @throws Exception an exception when an error occurs.
     */
    private ClientInterceptor[] addInterceptors() throws Exception {
        ClientInterceptor[] interceptors = new ClientInterceptor[2];

        EndPointInterceptor endPointInterceptor = new EndPointInterceptor();
        interceptors[0] = endPointInterceptor;
        interceptors[1] = xwsSecurityInterceptor;

        return interceptors;
    }

    /**
     * An ActionCallback is responsible for sending the correct WS-Addressing details like FROM, TO etc.
     * @param fromURL the url that the call came from
     * @return a callback
     * @throws URISyntaxException
     */
    private ActionCallback createActionCallback(String fromURL) throws URISyntaxException {
        EndpointReference from = new EndpointReference(new URI(wsaPrefix + constantValues.getFromOIN()));
        URI to = new URI(constantValues.getDestinationURL() + "?" + constantValues.getToOIN());
        ActionCallback actionCallback = new ActionCallback(new URI(fromURL), new AdressingVersionMustUnderstandDisabled(), to);
        actionCallback.setFrom(from);
        return actionCallback;
    }


    /**
     * Calls the webservice.
     * @param webServiceTemplate the template to use for calling the webservice
     * @param actionCallback actioncallback for adding WS-Addressing headers
     * @param request the request object to send to the webservice
     * @return the response from the webservice
     */
    private Bedrijfsdocument callWebservice(WebServiceTemplate webServiceTemplate, ActionCallback actionCallback, Object request) {
        return (Bedrijfsdocument) webServiceTemplate.marshalSendAndReceive(request, actionCallback);
    }

    private PoolingHttpClientConnectionManager createConnectionManager(Registry<ConnectionSocketFactory> registry, SoapConfiguration soapConfiguration) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        connectionManager.setMaxTotal(connectionManager.getMaxTotal());
        connectionManager.setDefaultMaxPerRoute(soapConfiguration.getConnectionPoolConfiguration().getMaxConnectionsPerHost());
        connectionManager.setDefaultSocketConfig(SocketConfig.custom()
                .setSoTimeout(soapConfiguration.getConnectionPoolConfiguration().getReadTimeout()).build());
        return connectionManager;
    }
}
