package nl.craftsmen.duo.webservice.client;

import nl.craftsmen.duo.webservice.data.Bedrijfsdocument;
import nl.craftsmen.duo.webservice.data.ExampleRequest;
import nl.craftsmen.duo.webservice.data.WebserviceResponse;
import org.springframework.beans.factory.annotation.Autowired;

public class ExampleClient  {

    @Autowired
    private WebserviceClient webserviceClient;

    private void send() {
        ExampleRequest requestObject = new ExampleRequest("Peter", 19);
        WebserviceResponse response = webserviceClient.send(requestObject, "/myserver/");
        Bedrijfsdocument bedrijfsdocument = response.getResponseObject();
    }

}
