package nl.craftsmen.duo.webservice.soaputils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.WebServiceMessage;

/**
 * Utility class for storing the SOAP/XML request and response
 */
@Slf4j
public class SoapXMLHelper {

    private static final String START_IDENTIFICATION_TAG = "<ns2:identificatiecodeBedrijfsdocument>";
    private static final String END_IDENTIFICATION_TAG = "</ns2:identificatiecodeBedrijfsdocument>";
    private static final String START_IDENTIFICATION_TAG2 = "<identificatiecodeBedrijfsdocument>";
    private static final String END_IDENTIFICATION_TAG2 = "</identificatiecodeBedrijfsdocument>";

    private static HashMap<String, String> soapRequestXmlMessages = new HashMap<>();
    private static HashMap<String, String> soapResponseXmlMessages = new HashMap<>();

    /**
     * Method for converting the WebServiceMessage to a String containing the SOAP/XML
     * @param webServiceMessage the webservice message used for sending the message
     * @return a string containing the SOAP/XML
     */
    public static String getSoapXmlMessage(WebServiceMessage webServiceMessage)  {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            webServiceMessage.writeTo(bos);
            return bos.toString("UTF-8");
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    /**
     * Adds a SOAP/XML request to the story
     * @param webServiceMessage the webservice message containing the SOAP/XML
     */
    public static void addSoapRequestXml(WebServiceMessage webServiceMessage) {
        String soapXmlMessage = SoapXMLHelper.getSoapXmlMessage(webServiceMessage);
        if (soapXmlMessage != null) {
            String key = getIdentificationFromSoapXmlMessage(soapXmlMessage);
            soapRequestXmlMessages.put(key, soapXmlMessage);
        }
    }

    /**
     * Retrieves the SOAP/XML request for the supplied key
     * @param key the key
     * @return the SOAP/XML for the supplied key or <code>null</code> if not found
     */
    public static String getSoapRequestXml(String key) {
        if (key != null) {
            return soapRequestXmlMessages.get(key);
        }
        return null;
    }

    /**
     * Adds a SOAP/XML response to the story
     * @param webServiceMessage the webservice message containing the SOAP/XML
     */
    public static void addSoapResponseXml(WebServiceMessage webServiceMessage) {
        String soapXmlMessage = SoapXMLHelper.getSoapXmlMessage(webServiceMessage);
        if (soapXmlMessage != null) {
            String key = getIdentificationFromSoapXmlMessage(soapXmlMessage);
            soapResponseXmlMessages.put(key, soapXmlMessage);
        }
    }

    /**
     * Adds a SOAP/XML response to the story
     * @param webServiceMessage the webservice message containing the SOAP/XML
     */
    // tweede versie van deze method om key expliciet te kunnen meegeven
    public static void addSoapResponseXml(WebServiceMessage webServiceMessage,String identificatieBedrijfsdocument) {
        String soapXmlMessage = SoapXMLHelper.getSoapXmlMessage(webServiceMessage);
        if (soapXmlMessage != null) {
            soapResponseXmlMessages.put(identificatieBedrijfsdocument, soapXmlMessage);
        }
    }

    /**
     * Retrieves the SOAP/XML response for the supplied key
     * @param key the key
     * @return the SOAP/XML for the supplied key or <code>null</code> if not found
     */
    public static String getSoapResponseXml(String key) {
        if (key != null) {
            return soapResponseXmlMessages.get(key);
        }
        return null;
    }

    /**
     * Method for retrieving an identification from the SOAP/XML in order to correlate the soap messages
     * to the original request sent.
     * @param soapRequest the SOAP/XML sent/received (could be request or response)
     * @return the number for IDENTIFICATIE_BEDRIJFSDOCUMENT
     */
    //private static String getIdentificationFromSoapXmlMessage(String soapRequest) {
        public static String getIdentificationFromSoapXmlMessage(String soapRequest) {
        //log.info("Dit is mijn soapRequest in soaphelperclass: "+soapRequest);
        String returnvalue;
        try {
            int startPosition = soapRequest.indexOf(START_IDENTIFICATION_TAG) + START_IDENTIFICATION_TAG.length();
            int endPosition = soapRequest.indexOf(END_IDENTIFICATION_TAG);
            returnvalue = soapRequest.substring(startPosition, endPosition);
        }
        catch(StringIndexOutOfBoundsException e) {
            try {
                int startPosition = soapRequest.indexOf(START_IDENTIFICATION_TAG2) + START_IDENTIFICATION_TAG2.length();
                int endPosition = soapRequest.indexOf(END_IDENTIFICATION_TAG2);
                returnvalue = soapRequest.substring(startPosition, endPosition);
            }
            catch (StringIndexOutOfBoundsException e2) {
                log.info("Onhandelbaar soaprequest: "+soapRequest);
                throw e2;
            }
        }

        //log.info("startPosition-endPosition "+ startPosition + "-" + endPosition);
        //if (endPosition < startPosition)
        //{return "BOGUS";}
        //return soapRequest.substring(startPosition, endPosition)
          return returnvalue;
    }

}
