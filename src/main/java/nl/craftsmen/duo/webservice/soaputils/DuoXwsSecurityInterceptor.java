package nl.craftsmen.duo.webservice.soaputils;

import nl.craftsmen.duo.webservice.data.ConstantValues;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.soap.security.xwss.XwsSecurityInterceptor;
import org.springframework.ws.soap.security.xwss.callback.KeyStoreCallbackHandler;

public class DuoXwsSecurityInterceptor extends XwsSecurityInterceptor {
    public static final String SECURITY_POLICY_XML_FILE_NAME = "securityPolicy.xml";
    public static final String CERTIFICAAT_ALIAS = "<<alias-used-for-importing-the-certificate-in-cacerts>> eg. my_cert_nl";

    public DuoXwsSecurityInterceptor() {
        super();
        SslConfiguration sslConfiguration = new SslConfiguration();
        setPolicyConfiguration(new ClassPathResource(SECURITY_POLICY_XML_FILE_NAME));
        setValidateResponse(false);
        setValidateRequest(false);

        KeyStoreCallbackHandler keyStoreCallbackHandler = new KeyStoreCallbackHandler();
        keyStoreCallbackHandler.setKeyStore(sslConfiguration.getKeystore());
        keyStoreCallbackHandler.setTrustStore(sslConfiguration.getTruststore());
        keyStoreCallbackHandler.setPrivateKeyPassword(ConstantValues.JAVA_KEYSTORE_PASSWORD);
        keyStoreCallbackHandler.setDefaultAlias(CERTIFICAAT_ALIAS);
        setCallbackHandler(keyStoreCallbackHandler);
        try {
            afterPropertiesSet();
        } catch (Exception e) {
            throw new IllegalStateException("Error occurred creating the XwsSecurityInterceptor");
        }
    }
}
