package nl.craftsmen.duo.webservice.soaputils;

import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.addressing.core.MessageAddressingProperties;
import org.springframework.ws.soap.addressing.version.Addressing10;

/**
 * This class is an override of Addressing10 since the action.setMustUnderstand was hardcoded set to true but the DUO
 * server does not recognize this tag.
 */
public class AdressingVersionMustUnderstandDisabled extends Addressing10 {

    @Override
    public void addAddressingHeaders(SoapMessage message, MessageAddressingProperties map) {
        SoapHeader header = message.getSoapHeader();
        header.addNamespaceDeclaration(this.getNamespacePrefix(), this.getNamespaceUri());
        SoapHeaderElement action;
        if(map.getTo() != null) {
            action = header.addHeaderElement(this.getToName());
            action.setText(map.getTo().toString());
            action.setMustUnderstand(false);
        }

        if(map.getFrom() != null) {
            action = header.addHeaderElement(this.getFromName());
            this.addEndpointReference(action, map.getFrom());
        }

        if(map.getReplyTo() != null) {
            action = header.addHeaderElement(this.getReplyToName());
            this.addEndpointReference(action, map.getReplyTo());
        }

        if(map.getFaultTo() != null) {
            action = header.addHeaderElement(this.getFaultToName());
            this.addEndpointReference(action, map.getFaultTo());
        }

        action = header.addHeaderElement(this.getActionName());
        action.setText(map.getAction().toString());
        SoapHeaderElement relatesTo;
        if(map.getMessageId() != null) {
            relatesTo = header.addHeaderElement(this.getMessageIdName());
            relatesTo.setText(map.getMessageId().toString());
        }

        if(map.getRelatesTo() != null) {
            relatesTo = header.addHeaderElement(this.getRelatesToName());
            relatesTo.setText(map.getRelatesTo().toString());
        }

        this.addReferenceNodes(header.getResult(), map.getReferenceParameters());
        this.addReferenceNodes(header.getResult(), map.getReferenceProperties());
    }}
