package nl.craftsmen.duo.webservice.soaputils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * An aggregator class to have a single entity to provide configuration to other classes.
 *
 * <p>
 *     This class uses the Builder pattern.
 * </p>
 */
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@Builder
public class SoapConfiguration {
    private final ConnectionPoolConfiguration connectionPoolConfiguration;
    private final SslConfiguration sslConfiguration;

    private final String destinationUrl;
}
