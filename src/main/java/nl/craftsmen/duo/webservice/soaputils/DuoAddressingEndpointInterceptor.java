package nl.craftsmen.duo.webservice.soaputils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import lombok.extern.slf4j.Slf4j;
import nl.craftsmen.duo.webservice.data.ConstantValues;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.Assert;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.addressing.core.EndpointReference;
import org.springframework.ws.soap.addressing.core.MessageAddressingProperties;
import org.springframework.ws.soap.addressing.messageid.MessageIdStrategy;
import org.springframework.ws.soap.addressing.messageid.UuidMessageIdStrategy;
import org.springframework.ws.soap.addressing.version.AddressingVersion;
import org.springframework.ws.soap.server.SoapEndpointInterceptor;
import org.springframework.ws.transport.WebServiceConnection;
import org.springframework.ws.transport.WebServiceMessageSender;

/**
 * This class is extracted from the AddressingEndpointInterceptor from Spring-WS.
 * Because the wsa:To and wsa:From were added by default by Spring there was no way to change these.
 *
 * Therefore the endpoint was changed to change the @Action (which added the ws-addressing headers to the response) into
 * @PayloadRoot and this DuoAddressingEndpointInterceptor was added to add the wsa-properties ourselves.
 */
@Slf4j
public class DuoAddressingEndpointInterceptor implements SoapEndpointInterceptor {

    private static final String wsaPrefix = "http://www.w3.org/2005/08/addressing/anonymous?";
    private static final Log logger = LogFactory.getLog(nl.craftsmen.duo.webservice.soaputils.DuoAddressingEndpointInterceptor.class);
    private final AddressingVersion version;
    private MessageIdStrategy messageIdStrategy = new UuidMessageIdStrategy();
    private WebServiceMessageSender[] messageSenders;
    private URI replyAction;
    private URI faultAction;

    private ConstantValues constantValues;

    public DuoAddressingEndpointInterceptor(ConstantValues constantValues) {
        this.constantValues = constantValues;
        this.version = new AdressingVersionMustUnderstandDisabled();
        try {
            URI action = new URI(constantValues.getFromURLBase());
            this.replyAction = action;
            this.faultAction = action;
        } catch (URISyntaxException uriException) {
            log.error("Error in creating reply and fault actions due to incorrect URI, ", uriException);
        }
    }

    public final boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
        Assert.isInstanceOf(SoapMessage.class, messageContext.getRequest());
        SoapMessage request = (SoapMessage)messageContext.getRequest();
        MessageAddressingProperties requestMap = this.version.getMessageAddressingProperties(request);
        if(!this.version.hasRequiredProperties(requestMap)) {
            this.version.addMessageAddressingHeaderRequiredFault((SoapMessage)messageContext.getResponse());
            return false;
        } else if(this.messageIdStrategy.isDuplicate(requestMap.getMessageId())) {
            this.version.addInvalidAddressingHeaderFault((SoapMessage)messageContext.getResponse());
            return false;
        } else {
            return true;
        }
    }

    public final boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
        return this.handleResponseOrFault(messageContext, false);
    }

    public final boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
        return this.handleResponseOrFault(messageContext, true);
    }

    private boolean handleResponseOrFault(MessageContext messageContext, boolean isFault) throws Exception {
        Assert.isInstanceOf(SoapMessage.class, messageContext.getRequest());
        Assert.isInstanceOf(SoapMessage.class, messageContext.getResponse());
        MessageAddressingProperties requestMap = this.version.getMessageAddressingProperties((SoapMessage)messageContext.getRequest());
        EndpointReference replyEpr = !isFault?requestMap.getReplyTo():requestMap.getFaultTo();
        if(this.handleNoneAddress(messageContext, replyEpr)) {
            return false;
        } else {
            SoapMessage reply = (SoapMessage)messageContext.getResponse();
            URI replyMessageId = this.getMessageId(reply);
            URI action = isFault?this.faultAction:this.replyAction;
            URI to = new URI(wsaPrefix + constantValues.getToOIN());
            EndpointReference from = new EndpointReference(new URI(wsaPrefix + constantValues.getFromOIN()));
            EndpointReference replyTo = new EndpointReference(to);
            EndpointReference faultTo = new EndpointReference(to);
            MessageAddressingProperties replyMap = new MessageAddressingProperties(to, from, replyTo, faultTo, action, replyMessageId);
            this.version.addAddressingHeaders(reply, replyMap);
            if(this.handleAnonymousAddress(messageContext, replyEpr)) {
                return true;
            } else {
                this.sendOutOfBand(messageContext, replyEpr);
                return false;
            }
        }
    }

    private boolean handleNoneAddress(MessageContext messageContext, EndpointReference replyEpr) {
        if(replyEpr != null && !this.version.hasNoneAddress(replyEpr)) {
            return false;
        } else {
            if(logger.isDebugEnabled()) {
                logger.debug("Request [" + messageContext.getRequest() + "] has [" + replyEpr + "] reply address; reply [" + messageContext.getResponse() + "] discarded");
            }

            messageContext.clearResponse();
            return true;
        }
    }

    private boolean handleAnonymousAddress(MessageContext messageContext, EndpointReference replyEpr) {
        if(this.version.hasAnonymousAddress(replyEpr)) {
            if(logger.isDebugEnabled()) {
                logger.debug("Request [" + messageContext.getRequest() + "] has [" + replyEpr + "] reply address; sending in-band reply [" + messageContext.getResponse() + "]");
            }
            return true;
        } else {
            return false;
        }
    }

    private void sendOutOfBand(MessageContext messageContext, EndpointReference replyEpr) throws IOException {
        if(logger.isDebugEnabled()) {
            logger.debug("Request [" + messageContext.getRequest() + "] has [" + replyEpr + "] reply address; sending out-of-band reply [" + messageContext.getResponse() + "]");
        }

        boolean supported = false;
        WebServiceMessageSender[] var4 = this.messageSenders;
        int var5 = var4.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            WebServiceMessageSender messageSender = var4[var6];
            if(messageSender.supports(replyEpr.getAddress())) {
                supported = true;
                WebServiceConnection connection = null;

                try {
                    connection = messageSender.createConnection(replyEpr.getAddress());
                    connection.send(messageContext.getResponse());
                    break;
                } finally {
                    messageContext.clearResponse();
                    if(connection != null) {
                        connection.close();
                    }

                }
            }
        }

        if(!supported && logger.isWarnEnabled()) {
            logger.warn("Could not send out-of-band response to [" + replyEpr.getAddress() + "]. " + "Configure WebServiceMessageSenders which support this uri.");
        }

    }

    private URI getMessageId(SoapMessage response) {
        URI responseMessageId = this.messageIdStrategy.newMessageId(response);
        if(logger.isTraceEnabled()) {
            logger.trace("Generated reply MessageID [" + responseMessageId + "] for [" + response + "]");
        }

        return responseMessageId;
    }

    public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) {
    }

    public boolean understands(SoapHeaderElement header) {
        return this.version.understands(header);
    }
}
