package nl.craftsmen.duo.webservice.soaputils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;

/**
 * Inteceptor which will catch the request before it is sent and will retrieve the response
 * after the message is sent.
 */
@Slf4j
public class EndPointInterceptor implements ClientInterceptor {

    /**
     * This method will intercept the message before it is sent and will convert the message to
     * an XML message and store it in the temporary SoapXmlHelper store.
     * @param messageContext the context used for sending the message
     * @return a boolean stating whether the process can proceed or not, in our case always <code>true</code>.
     * @throws WebServiceClientException an exception when sending the message
     */
    @Override
    public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
        SoapXMLHelper.addSoapRequestXml(messageContext.getRequest());
        return true;
    }

    /**
     * This method will intercept the response and will convert the response to an XML message
     * and store it in the temporary SoapXmlHelper store.
     * @param messageContext the context used for sending the message
     * @return a boolean stating whether the process can proceed or not, in our case always <code>true</code>.
     * @throws WebServiceClientException
     */
    @Override
    public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
        SoapXMLHelper.addSoapResponseXml(messageContext.getResponse());
        return true;
    }

    /**
     * No further implementation.
     * @param messageContext the context used for sending the message
     * @return a boolean stating whether the process can proceed or not, in our case always <code>true</code>.
     * @throws WebServiceClientException
     */
    @Override
    public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
        //log.info("in handle fault, XML van de response is nu: " + SoapXMLHelper.getSoapXmlMessage(messageContext.getResponse()));
        //log.info("in handle fault, XML van de request is nu: " + SoapXMLHelper.getSoapXmlMessage(messageContext.getRequest()));
        SoapXMLHelper.addSoapRequestXml(messageContext.getRequest());
        //De key uit het request gebruiken in geval van een SOAP-FAULT, want dan is de response geen Bedrijfsdocument
        //de key wordt dus expliciet meegegeven in dit geval
        SoapXMLHelper.addSoapResponseXml(messageContext.getResponse()
                                        ,SoapXMLHelper.getIdentificationFromSoapXmlMessage(SoapXMLHelper.getSoapXmlMessage(messageContext.getRequest())));
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Exception e) throws WebServiceClientException {
    }


}
