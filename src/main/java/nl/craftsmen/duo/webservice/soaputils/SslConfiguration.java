package nl.craftsmen.duo.webservice.soaputils;

import java.security.KeyStore;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HostnameVerifier;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import nl.craftsmen.duo.webservice.data.ConstantValues;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.ws.soap.security.support.KeyStoreFactoryBean;

/**
 * Describes the (mutual) SSL connection configuration items. These include:
 *
 * <ul>
 *     <li>keystore: the keystore for the SSL context</li>
 *     <li>keystorePassword: the password to go with the keystore</li>
 *     <li>truststore: the truststore for the SSL context</li>
 *     <li>trustStrategy: the truststrategy, which can be used to override the usual verification process (usually not required)</li>
 *     <li>protocol: the protocol for the connection encryption (TLS, SSL, etc; usually TLS)</li>
 *     <li>hostnameVerifier: hostname verification strategy</li>
 *     <li>onlyAllowNullCiphers: if set to true, the supported ciphers will be set to include only null ciphers</li>
 * </ul>
 *
 * <p>
 *     This class uses the Builder pattern.
 * </p>
 *
 * <p>
 *     Please note that:
 *     <ul>
 *         <li>if no protocol is provided, the TLS protocol will be used</li>
 *         <li>if no hostnameVerifier is provided, the {@link NoopHostnameVerifier#INSTANCE} will be used</li>
 *     </ul>
 * </p>
 *
 */
@Builder
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class SslConfiguration {
    private static final String TRUST_STORE_PASSWORD = "changeit";
    private static final String KEYSTORE_TYPE = "JKS";
    private static final String TRUST_STORE_LOCATION = "<<path-to-cacerts-file-from-JDK eq: C//Java//jdk1.8.0_92//jre//lib//security//cacerts>>";
    public static final String JAVA_KEYSTORE_LOCATION = "<<name-of-certificate-file>> eg. cert_duo_nl.jks";

    private Resource javaKeyStoreResource = new ClassPathResource(JAVA_KEYSTORE_LOCATION);

    public static final List<String> NULL_CIPHERS = Arrays.asList(
            "TLS_RSA_WITH_NULL_MD5",
            "TLS_RSA_WITH_NULL_SHA",
            "SSL_RSA_WITH_NULL_MD5",
            "SSL_RSA_WITH_NULL_SHA");
    public static final String TLS_PROTOCOL = "TLS";

    /** the keystore for the SSL context */
    private final KeyStore keystore;

    /** the truststore for the SSL context */
    private final KeyStore truststore;

    /** if set to true, the supported ciphers will be set to include only null cipherst */
    @Getter(AccessLevel.NONE)
    private final boolean onlyAllowNullCiphers;

    public String[] getSupportedCiphers() {
        return onlyAllowNullCiphers ? NULL_CIPHERS.toArray(new String[NULL_CIPHERS.size()]) : null;
    }

    public String getProtocol() {
        return TLS_PROTOCOL;
    }

    public HostnameVerifier getHostnameVerifier() {
        return NoopHostnameVerifier.INSTANCE ;
    }

    public SslConfiguration() {
        this.keystore = createKeyStore();
        this.truststore = createTrustStore();
        this.onlyAllowNullCiphers = false;
    }

    private KeyStore createKeyStore() {
        try {
            KeyStoreFactoryBean keyStoreFactoryBean = new KeyStoreFactoryBean();
            keyStoreFactoryBean.setLocation(javaKeyStoreResource);
            keyStoreFactoryBean.setPassword(ConstantValues.JAVA_KEYSTORE_PASSWORD);
            keyStoreFactoryBean.setType(KEYSTORE_TYPE);
            keyStoreFactoryBean.afterPropertiesSet();
            return keyStoreFactoryBean.getObject();
        } catch (Exception exc) {
            throw new RuntimeException("Error creating keystore after properties set", exc);
        }
    }

    private KeyStore createTrustStore() {
        try {
            KeyStoreFactoryBean trustStoreFactoryBean = new KeyStoreFactoryBean();
            trustStoreFactoryBean.setLocation(new FileSystemResource(TRUST_STORE_LOCATION));
            trustStoreFactoryBean.setPassword(TRUST_STORE_PASSWORD);
            trustStoreFactoryBean.setType(KEYSTORE_TYPE);
            trustStoreFactoryBean.afterPropertiesSet();
            return trustStoreFactoryBean.getObject();
        } catch (Exception exc) {
            throw new RuntimeException("Error creating trust store after properties set", exc);
        }
    }

    public char[] getKeyPassword() {
        return ConstantValues.JAVA_KEYSTORE_PASSWORD == null ? null : ConstantValues.JAVA_KEYSTORE_PASSWORD.toCharArray();
    }
}
