package nl.craftsmen.duo.webservice.soaputils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;

@Slf4j
public class SoapActionInterceptor implements EndpointInterceptor {

    @Override
    public boolean handleRequest(MessageContext messageContext, Object methodEndpoint) throws Exception {
        //log.info("In handle request met als messageContext.getRequest: "+ messageContext.getRequest().toString());
        SoapXMLHelper.addSoapRequestXml(messageContext.getRequest());
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object methodEndpoint) throws Exception {
        SoapXMLHelper.addSoapResponseXml(messageContext.getResponse());
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object methodEndpoint) throws Exception {
        SoapXMLHelper.addSoapRequestXml(messageContext.getRequest());
        SoapXMLHelper.addSoapResponseXml(messageContext.getResponse());
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object methodEndpoint, Exception exception) throws Exception {
        // Do nothing
    }
}
