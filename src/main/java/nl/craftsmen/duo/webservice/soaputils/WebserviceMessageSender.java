package nl.craftsmen.duo.webservice.soaputils;

import java.io.IOException;
import java.net.URI;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.springframework.ws.transport.WebServiceConnection;
import org.springframework.ws.transport.http.AbstractHttpWebServiceMessageSender;
import org.springframework.ws.transport.http.HttpComponentsConnection;

public class WebserviceMessageSender extends AbstractHttpWebServiceMessageSender {
    private HttpClient httpClient;
    private HttpContext httpContext;


    public WebserviceMessageSender(HttpClient httpClient) {
        this.httpClient = httpClient;
        this.httpContext = new BasicHttpContext();
    }

    public WebServiceConnection createConnection(URI uri) throws IOException {
        return new WebserviceMessageSender.DuoHttpComponentConnections(this.httpClient, uri, httpContext);
    }

    private static class DuoHttpComponentConnections extends HttpComponentsConnection {
        public DuoHttpComponentConnections(HttpClient httpClient, URI uri, HttpContext httpContext) {
            super(httpClient, new HttpPost(uri), httpContext);
        }
    }


}
