package nl.craftsmen.duo.webservice.soaputils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * Describes the connection (pool) configuration items. These include
 *
 * <ul>
 *     <li><code>maxTotalConnections</code>: the maximum number of connections over all hosts combined</li>
 *     <li><code>maxConnectionsPerHost</code>: the maximum number of connections per host</li>
 *     <li><code>readTimeout</code>: the timeout on the socket to read the response</li>
 *     <li><code>connectTimeout</code>: the timeout on the connection pool to obtain a connection</li>
 * </ul>
 *
 * <p>
 *     This class uses the Builder pattern.
 * </p>
 *
 */
@Builder
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ConnectionPoolConfiguration {
    /** The maximum number of connections over all hosts combined */
    private final int maxTotalConnections;
    /** The maximum number of connections per host */
    private final int maxConnectionsPerHost;
    /** The timeout on the socket to read the response */
    private final int readTimeout;
    /** The timeout on the connection pool to obtain a connection */
    private final int connectTimeout;
}
