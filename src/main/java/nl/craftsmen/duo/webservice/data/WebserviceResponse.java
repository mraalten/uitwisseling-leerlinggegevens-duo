package nl.craftsmen.duo.webservice.data;

import lombok.Getter;

/**
 * This class will hold the response object as returned by the webservice and will hold the SOAP/XML for the Request and the Response
 */
@Getter
public class WebserviceResponse {

    private Bedrijfsdocument responseObject;
    private String requestXML;
    private String responseXml;
    private boolean success;

    /**
     * Construct a response object indicating success
     * @param responseObject the response object from the webservice call
     * @param requestXML the xml for the request sent to the webservice
     * @param responseXml the xml for the response returned from the webservice
     */
    public WebserviceResponse(Bedrijfsdocument responseObject, String requestXML, String responseXml) {
        this.responseObject = responseObject;
        this.requestXML = requestXML;
        this.responseXml = responseXml;
        this.success = true;
    }

    /**
     * Construct a response object indicating errors
     * @param requestXML the xml for the request sent to the webservice
     * @param responseXml the xml for the response returned from the webservice
     */
    public WebserviceResponse(String requestXML, String responseXml) {
        this.requestXML = requestXML;
        this.responseXml = responseXml;
        this.success = false;
    }
}
