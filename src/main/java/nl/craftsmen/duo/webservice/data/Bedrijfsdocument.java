
package nl.craftsmen.duo.webservice.data;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * 
 * Definitie: Een pakket gestructureerde gegevens
 * DefinitieHerkomst: DUO
 * 
 * 
 * <p>Java class for Bedrijfsdocument complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Bedrijfsdocument"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="identificatiecodeBedrijfsdocument" type="{http://duo.nl/schema/DUO_MBOaanlevering_V1}IdentificatiecodeBedrijfsdocument-v02"/&gt;
 *         &lt;element name="verzendendeInstantie" type="{http://duo.nl/schema/DUO_MBOaanlevering_V1}VerzendendeInstantie"/&gt;
 *         &lt;element name="ontvangendeInstantie" type="{http://duo.nl/schema/DUO_MBOaanlevering_V1}OntvangendeInstantie"/&gt;
 *         &lt;element name="datumTijdBedrijfsdocument" type="{http://duo.nl/schema/DUO_MBOaanlevering_V1}DatumTijdBedrijfsdocument"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bedrijfsdocument", propOrder = {
    "identificatiecodeBedrijfsdocument",
    "verzendendeInstantie",
    "ontvangendeInstantie",
    "datumTijdBedrijfsdocument"
})
@XmlSeeAlso({
})
public class Bedrijfsdocument
    implements Serializable
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String identificatiecodeBedrijfsdocument;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String verzendendeInstantie;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String ontvangendeInstantie;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datumTijdBedrijfsdocument;

    /**
     * Gets the value of the identificatiecodeBedrijfsdocument property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatiecodeBedrijfsdocument() {
        return identificatiecodeBedrijfsdocument;
    }

    /**
     * Sets the value of the identificatiecodeBedrijfsdocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatiecodeBedrijfsdocument(String value) {
        this.identificatiecodeBedrijfsdocument = value;
    }

    /**
     * Gets the value of the verzendendeInstantie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerzendendeInstantie() {
        return verzendendeInstantie;
    }

    /**
     * Sets the value of the verzendendeInstantie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerzendendeInstantie(String value) {
        this.verzendendeInstantie = value;
    }

    /**
     * Gets the value of the ontvangendeInstantie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOntvangendeInstantie() {
        return ontvangendeInstantie;
    }

    /**
     * Sets the value of the ontvangendeInstantie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOntvangendeInstantie(String value) {
        this.ontvangendeInstantie = value;
    }

    /**
     * Gets the value of the datumTijdBedrijfsdocument property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumTijdBedrijfsdocument() {
        return datumTijdBedrijfsdocument;
    }

    /**
     * Sets the value of the datumTijdBedrijfsdocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumTijdBedrijfsdocument(XMLGregorianCalendar value) {
        this.datumTijdBedrijfsdocument = value;
    }

    public Bedrijfsdocument withIdentificatiecodeBedrijfsdocument(String value) {
        setIdentificatiecodeBedrijfsdocument(value);
        return this;
    }

    public Bedrijfsdocument withVerzendendeInstantie(String value) {
        setVerzendendeInstantie(value);
        return this;
    }

    public Bedrijfsdocument withOntvangendeInstantie(String value) {
        setOntvangendeInstantie(value);
        return this;
    }

    public Bedrijfsdocument withDatumTijdBedrijfsdocument(XMLGregorianCalendar value) {
        setDatumTijdBedrijfsdocument(value);
        return this;
    }

}
