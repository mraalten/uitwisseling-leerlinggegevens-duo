package nl.craftsmen.duo.webservice.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@Builder
@AllArgsConstructor
public class ConstantValues {
    public static final String JAVA_KEYSTORE_PASSWORD = "<<fake-password>>";
    private String fromOIN;
    private String toOIN;
    private String BRIN;
    private String ontvangenInstantie;
    private String fromURLBase;
    private String destinationURL;

    public ConstantValues() {
    }

    @Override
    public String toString() {
        return "ConstantValues{"
                + "fromOIN='" + fromOIN + '\''
                + ", toOIN='" + toOIN + '\''
                + ", BRIN='" + BRIN + '\''
                + ", ontvangenInstantie='" + ontvangenInstantie + '\''
                + ", fromURLBase='" + fromURLBase + '\''
                + ", destinationURL='" + destinationURL + '\''
                + '}';
    }
}
