package nl.craftsmen.duo.webservice.data;

public class ExampleRequest {

    private String name;
    private int age;

    public ExampleRequest(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
